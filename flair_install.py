from flair.data import Sentence
from flair.models import SequenceTagger

# make a sentence
sentence = Sentence("J'aime Paris, Lyon et la saucisse!")

# load the NER tagger
tagger = SequenceTagger.load('pos-multi')

# run NER over sentence
tagger.predict(sentence)

print(sentence)
print('Noms propres trouvés:')

# iterate over entities and print
for entity in sentence.get_spans('pos-multi'):
    print(entity)

